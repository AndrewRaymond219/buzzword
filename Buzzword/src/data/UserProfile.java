package data;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @Author Andrew Raymond
 */
public class UserProfile {
    String       Username;
    String       Password;
    int[]        LevelData;
    GameData     gamedata;
    GameDataFile gameDataFile;

    public UserProfile(){
        this.Username = new String();
        this.Password = new String();
    }

    public UserProfile(String username, String password, GameData gamedata, GameDataFile gameDataFile){
        this.Username       = username;
        this.Password       = password;
        this.LevelData      = gamedata.getLevelData();
        this.gamedata       = gamedata;
        this.gameDataFile   = gameDataFile;
    }

    public String getUsername(){
        return Username;
    }

    public String getPassword(){
        return Password;
    }

    public void   setPassword(String password) { this.Password = password; }

    public void   setUsername(String newName) { this.Username = newName; }

    public boolean storeProfile(){          //Returns true if the account was created, and false if the account was already found...
        String pathStr = "out\\savedata\\" + Username + ".json";
        Path path = Paths.get(pathStr);
        if(gameDataFile.checkAcc(path)){    //Check if the path exists already, if so, then there is already an account with this name notify the user
            return false;
        } else {
            gameDataFile.storeAcc(gamedata, path);
            return true;
        }
    }

    public boolean updateProfile(){
        String pathStr = "out\\savedata\\" + Username + ".json";
        Path path = Paths.get(pathStr);
        if (gameDataFile.checkAcc(path)){
            gameDataFile.storeAcc(gamedata, path);
            return true;
        } else {
            System.out.println("INCORRECT ACCOUNT NAME");
            return false;
        }
    }
}
