package data;

import com.fasterxml.jackson.core.*;
import components.AppDataComponent;
import components.AppFileComponent;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.spec.KeySpec;
import java.util.Base64;

/**
 * @author Andrew Raymond
 */
public class GameDataFile implements AppFileComponent {
    //Encryption Inits:
    private static final String UNICODE_FORMAT = "UTF8";
    public static  final String DESEDE_ENCRYPTION_SCHEME = "DESede";
    private String myEncryptionKey    = "ThisIsAndrewThisIsAndrew";
    private String myEncryptionScheme = DESEDE_ENCRYPTION_SCHEME;
    private byte[] arrayBytes;
    private KeySpec ks;
    private SecretKeyFactory skf;
    private Cipher cipher;
    SecretKey key;
    private String encryptedString = null;

    public static final String USERNAME     = "USERNAME";
    public static final String PASSWORD     = "PASSWORD";
    public static final String LEVEL_DATA    = "LEVEL_DATA";

    public void initEncrypt() throws Exception{
        arrayBytes = myEncryptionKey.getBytes(UNICODE_FORMAT);
        ks = new DESedeKeySpec(arrayBytes);
        skf = SecretKeyFactory.getInstance(myEncryptionScheme);
        cipher = Cipher.getInstance(myEncryptionScheme);
        key = skf.generateSecret(ks);
    }


    @Override
    public void storeAcc(AppDataComponent data, Path to){
        GameData    gamedata    =   (GameData) data;
        JsonFactory jsonFactory = new JsonFactory();
        int[]       levelData   = gamedata.getLevelData();

        try (OutputStream out =  Files.newOutputStream(to)) {   //with "to" being "username"
            JsonGenerator generator = jsonFactory.createGenerator(out, JsonEncoding.UTF8);
            generator.writeStartObject();

            generator.writeStringField(USERNAME, gamedata.getWorkingProfile().getUsername());

            //PASSWORD ENCRYPTION
            try {
                this.initEncrypt();
            } catch (Exception e){
                e.printStackTrace();
            }
            encryptedString = this.encrypt(gamedata.getWorkingProfile().getPassword());

            generator.writeStringField(PASSWORD, encryptedString);

            generator.writeFieldName(LEVEL_DATA);
            generator.writeStartArray(levelData.length);
            for (int i : levelData)
                generator.writeNumber(i);
            generator.writeEndArray();

            generator.writeEndObject();

            generator.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

    }

    public boolean checkAcc(Path to){
        if (Files.exists(to)){ //The file already exists, so check whether or not it has the correct password
            return true;
        } else {   //This means that the file doesn't exist
            return false;
        }
    }

    public void loginAcc(AppDataComponent data, Path from){
        GameData gamedata = (GameData) data;

    }
    @Override
    public void saveData(AppDataComponent data, Path to) {

    }

    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException {

    }

    public void changeUsername(String newUsername, Path from){
        JsonFactory jsonFactory = new JsonFactory();

        try (OutputStream out =  Files.newOutputStream(from)) {   //with "to" being "username"
            JsonGenerator generator = jsonFactory.createGenerator(out, JsonEncoding.UTF8);
            generator.writeStartObject();

            generator.writeStringField(USERNAME, newUsername);

            generator.writeEndObject();

            generator.close();

        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        /*
        JsonFactory jsonFactory = new JsonFactory();
        try{
            JsonParser  jsonParser  = jsonFactory.createParser(Files.newInputStream(from));
            while (!jsonParser.isClosed()) {
                JsonToken token = jsonParser.nextToken();
                if (JsonToken.FIELD_NAME.equals(token)) {
                    String fieldname = jsonParser.getCurrentName();
                    switch (fieldname) {
                        case USERNAME:
                            jsonParser.nextToken();
                            jsonParser.clearCurrentToken();
                            jsonParser.(newUsername);
                            break;

                        default:
                            throw new JsonParseException(jsonParser, "Unable to load JSON data");
                    }
                }
            }
        } catch (IOException e){}
        */
    }

    //Returns 1 if the password is the same, return 0 if not.
    public boolean checkPassword(AppDataComponent data, Path from, String password, GameDataFile file) {
        GameData gamedata = (GameData) data;
        String   loadedUN = "";
        String   loadedPW;

        JsonFactory jsonFactory = new JsonFactory();
        try{
            JsonParser  jsonParser  = jsonFactory.createParser(Files.newInputStream(from));

            while (!jsonParser.isClosed()) {
                JsonToken token = jsonParser.nextToken();
                if (JsonToken.FIELD_NAME.equals(token)) {
                    String fieldname = jsonParser.getCurrentName();
                    switch (fieldname) {
                        case USERNAME:
                            jsonParser.nextToken();
                            loadedUN = jsonParser.getValueAsString();
                            break;
                        case PASSWORD:
                            jsonParser.nextToken();
                            String temp = jsonParser.getValueAsString();
                            if (temp.isEmpty()){

                            } else {
                                 temp  = decrypt(temp);
                            }
                            if (temp.equals(password)){
                                //If the password is correct, load in the data
                                loadedPW = jsonParser.getValueAsString();
                                gamedata.initLevelData();

                                //SET THE MAX LEVEL IN GAMEDATA
                                int i = 0;
                                while (JsonToken.END_ARRAY != jsonParser.nextToken()){
                                    String text = jsonParser.getText();
                                    if (text.equals("LEVEL_DATA") || text.equals("[")){
                                        jsonParser.nextToken();
                                    } else {
                                        gamedata.setMaxLevel(i, Integer.valueOf(jsonParser.getText().toString()));
                                        i++;
                                    }
                                }

                                UserProfile newProf = new UserProfile(loadedUN, loadedPW, gamedata, file);
                                gamedata.setWorkingProfile(newProf);
                                return true;
                            } else {
                                return false;
                            }
                        default:
                            throw new JsonParseException(jsonParser, "Unable to load JSON data");
                    }
                }
            }
        } catch (IOException e){}
        return false;
    }

    public String encrypt(String pass) {
        String encrypted = null;
        try {
            cipher.init(Cipher.ENCRYPT_MODE, key);
            byte[] plain = pass.getBytes(UNICODE_FORMAT);
            byte[] encryptedTxt = cipher.doFinal(plain);
            encrypted = new String(Base64.getEncoder().encode(encryptedTxt));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encrypted;
    }

    public String decrypt(String encryptedPass){
        String decrypted = null;
        try{
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] encryptedTxt = Base64.getDecoder().decode(encryptedPass.getBytes());
            byte[] plain = cipher.doFinal(encryptedTxt);
            decrypted = new String(plain);
        } catch (Exception e){
            return "";
        }
        return decrypted;
    }


    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException { }
}
