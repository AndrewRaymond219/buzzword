package data;

import apptemplate.AppTemplate;
import components.AppDataComponent;
import controller.GameError;
import javafx.scene.control.Button;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.GridPane;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Stream;

/**
 * @author Andrew Raymond
 */
public class GameData implements AppDataComponent {
    private static final int TOTAL_NUMBER_OF_STORED_WORDS = 349900;
    private static final int TOTAL_NUMBER_OF_CATEGORY_WORDS = 20;

    public  AppTemplate         appTemplate;
    public  int                 currentWorkspace;
    private boolean             illegalChar;
    private int[]               levelData;
    public  ArrayList<String>   levelTargets;
    private UserProfile         workingProfile;
    private int                 currentScore;
    private int                 targetScore;
    public  String              stringBuffer = "";
    public  ArrayList<Button>   buttonBuffer = new ArrayList<>();
    public boolean              isWon = false;

    public GameData(AppTemplate appTemplate) {
        this(appTemplate, false);
    }

    public GameData(AppTemplate appTemplate, boolean initiateGame) {
        if (initiateGame) {
            this.levelTargets = new ArrayList<>();
            this.workingProfile = new UserProfile();
            this.levelData = new int[4];
            this.currentScore = 0;
            this.targetScore = 0;
            this.currentWorkspace = 0;
            initLevelData();

        } else {
            this.appTemplate = appTemplate;
        }
    }

    public void initLevelData() {
        this.levelData = new int[4];
        for (int i = 0; i < 4; i++) {
            levelData[i] = 1;
        }
        this.levelTargets = new ArrayList<>();
    }

    public void gameWon(){
        this.isWon = true;
        System.out.println("WON");
    }

    public void gameLost(){
        this.isWon = false;
        System.out.println("LOST");
    }

    @Override
    public void reset() {
        appTemplate.getWorkspaceComponent().reloadWorkspace();
    }

    public void addToCurrentScore(int add){
        this.currentScore += add;
    }

    public int  getCurrentScore(){
        return this.currentScore;
    }

    public int  getTargetScore(){ return this.targetScore; }

    public void setCurrentScore(int currentScore) { this.currentScore = currentScore; }

    public void setTargetScore(int targetScore){ this.targetScore = targetScore; }

    public void addToButtonBuffer(Button buffer){
        this.buttonBuffer.add(buffer);
    }

    public void clearButtonBuffer(){
        this.buttonBuffer.clear();
    }

    public void addToStringBuffer(String charToAdd) {
        this.stringBuffer += charToAdd;
    }

    public void clearStringBuffer(){
        this.stringBuffer = new String("");
    }

    public boolean checkStringBuffer(ArrayList<String> tempList){
        for (int i = 0; i < tempList.size(); i++){
            if (stringBuffer.equals(tempList.get(i).toString())){
                //At least one of the words has been correctly highlighted
                //Need to make sure that it is also in the correct position
                int currentScore = tempList.get(i).length()*5;

                addToCurrentScore(currentScore);
                return true;
            }
        }
        //learStringBuffer();
        return false;
    }

    public String getLevelTarget(int catNumber){
        URL wordsResource = getClass().getClassLoader().getResource("words/" + catNumber + ".txt");
        assert wordsResource != null;

        int toSkip = new Random().nextInt(TOTAL_NUMBER_OF_CATEGORY_WORDS);
        try (Stream<String> lines = Files.lines(Paths.get(wordsResource.toURI()))) {
            String temp = lines.skip(toSkip).findFirst().get();
            for (int i = 0; i < temp.length(); i++) {
                if (temp.charAt(i) < 97 || temp.charAt(i) > 122) {
                    illegalChar = true;
                }
            }

            if (illegalChar) {
                getLevelTarget(catNumber);    //try again because it had an invalid character
            } else {
                return temp;
            }

        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
            System.exit(1);
        }

        throw new GameError("Unable to load initial target word.");
    }

    public UserProfile getWorkingProfile() {
        return this.workingProfile;
    }

    public void setCurrentWorkspace(int newWorkspace){
        this.currentWorkspace = newWorkspace;
    }

    public int getCurrentWorkspace(){
        return currentWorkspace;
    }

    public void setWorkingProfile(UserProfile prof) {
        this.workingProfile = prof;
    }

    public int[] getLevelData() {
        return levelData;
    }

    public void setLevelData(int[] levelData) {
        this.levelData = levelData;
    }

    public ArrayList<String> getLevelTargets(){ return levelTargets; }

    public void unlockLevel(int category) {
        if (levelData[category] < 4) {
            levelData[category] = (levelData[category] + 1);
        }
    }

    public int getMaxLevel(int category) {
        return levelData[category];
    }

    public void setMaxLevel(int category, int maxLevel) {
        levelData[category] = maxLevel;
    }

}
