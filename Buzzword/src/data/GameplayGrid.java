package data;

import components.AppDataComponent;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

/**
 * @Author Andrew Raymond
 */
public class GameplayGrid {
    private GridPane            gameplayPane;
    private ArrayList<Button>   buttonArray;
    private ArrayList<ArrayList<Button>> keyboardBuffers = new ArrayList<>();
    private ArrayList<String>   solvedArray = new ArrayList<>();
    private int                 targetScore;
    private int                 currentScore;
    public  VBox                scoreBox = new VBox(new Label("GUESSED WORDS | SCORE"));
    private int                 numOfKeys = 0;
    private ArrayList<Integer>  currentSurrounding = new ArrayList<>();
    public  ArrayList<String>   correctGuesses = new ArrayList<>();
    private static int          GRID_WIDTH = 5;
    private GameData            gamedata;

    public GameplayGrid(AppDataComponent data, int categoryNum, int difficultyLevel) {
        scoreBox.getChildren().get(0).getStyleClass().addAll("dark", "white-text", "larger");
        gamedata = (GameData) data;
        targetScore = 0;
        currentScore = 0;

        //First init the GridPane and buttonArray
        gameplayPane = new GridPane();
        gameplayPane.setAlignment(Pos.CENTER);
        gameplayPane.setVgap(35);
        gameplayPane.setHgap(35);

        initLevelTargets(categoryNum, difficultyLevel);
        initButtonArray();

        //POPULATING THE ARRAY with TARGETS
        for (int i = 0; i < gamedata.levelTargets.size(); i++){
            buildWord(gamedata.levelTargets.get(i), findOpenPos());
        }

        fillEmptyCells();

        System.out.println("Words to add to grid: " + gamedata.levelTargets.toString());    //TESTING
        System.out.println("Size: " + gamedata.levelTargets.size());
        System.out.println("Target Score: " + targetScore);
        gameplayPane.setFocusTraversable(true);
        resetGameplayGrid(gamedata);
    }

    public void displaySolvedArray(){
        for (int i = 0; i < GRID_WIDTH*GRID_WIDTH; i++){
                if (!solvedArray.get(i).equals("-1") && !buttonArray.get(i).getStyle().equals("-fx-text-fill: green; -fx-background-color: white")){
                    buttonArray.get(i).setStyle("-fx-text-fill: red; -fx-background-color: white");
                }
            }
        }

    private void initLevelTargets(int categoryNum, int difficultyLevel){
        //Now read in the target words
        /*DIFFICULTY 1: ONE WORD    <5 LETTERS
          DIFFICULTY 2: TWO WORDS   <5 LETTERS
          DIFFICULTY 3: TWO WORDS   <6 LETTERS
          DIFFICULTY 4: THREE WORDS <6 LETTERS
        */
        if (difficultyLevel == 1 || difficultyLevel == 2) {
            for (int i = 0; i < difficultyLevel; i++) {
                String temp = gamedata.getLevelTarget(categoryNum);
                while (gamedata.levelTargets.contains(temp) || temp.length() > 4) {
                    temp = gamedata.getLevelTarget(categoryNum);
                }
                gamedata.levelTargets.add(temp);
                targetScore += (temp.length() * 5);
                gamedata.setTargetScore(targetScore);
            }
        } else {
            for (int i = 0; i < difficultyLevel - 1; i++) {
                String temp = gamedata.getLevelTarget(categoryNum);
                while (gamedata.levelTargets.contains(temp) || temp.length() > 5) {
                    temp = gamedata.getLevelTarget(categoryNum);
                }
                gamedata.levelTargets.add(temp);
                targetScore += (temp.length() * 5);
                gamedata.setTargetScore(targetScore);
            }
        }
    }

    private void initButtonArray() {
        buttonArray = new ArrayList<>(GRID_WIDTH * GRID_WIDTH);
        for (int i = 0; i < GRID_WIDTH * GRID_WIDTH; i++) {
            Button tempButton = new Button("-1");
            tempButton.getStyleClass().add("metro-button");
            tempButton.setId("" + i);
            buttonArray.add(tempButton);
            ArrayList<String> tempList = new ArrayList<>();
            for (int j = 0; j < gamedata.levelTargets.size(); j++) {
                tempList.add(j, gamedata.levelTargets.get(j));
            }

//=============================== MOUSE DRAG HANDLERS BELOW ===============================
            //When the mouse is pressed
            buttonArray.get(i).setOnMousePressed(e -> {
                if (!tempButton.getStyle().equals("-fx-text-fill: green; -fx-background-color: white")) {
                    gamedata.addToButtonBuffer(tempButton);
                    gamedata.addToStringBuffer(tempButton.getText());
                    tempButton.setStyle("-fx-text-fill: red; -fx-background-color: white");
                }
            });

            //When the mouse is released
            buttonArray.get(i).setOnMouseReleased(e -> {
                if (!tempButton.getStyle().equals("-fx-text-fill: green; -fx-background-color: white")) {
                    tempButton.setStyle("-fx-text-fill: black; -fx-background-color: #9EA5BC");
                    clearBuffers();
                }
            });

            gameplayPane.setOnMouseDragExited(e -> {
                for (int j = 0; j < buttonArray.size(); j++) {
                    if (!buttonArray.get(j).getStyle().equals("-fx-text-fill: green; -fx-background-color: white")) {
                        buttonArray.get(j).setStyle("-fx-text-fill: black; -fx-background-color: #9EA5BC");
                    }
                }
                clearBuffers();
            });

            gameplayPane.setOnMouseDragReleased(e -> {
                for (int j = 0; j < buttonArray.size(); j++) {
                    if (!buttonArray.get(j).getStyle().equals("-fx-text-fill: green; -fx-background-color: white")) {
                        buttonArray.get(j).setStyle("-fx-text-fill: black; -fx-background-color: #9EA5BC");
                    }
                }
                if (gamedata.checkStringBuffer(tempList)) {
                    for (int l = 0; l < gamedata.buttonBuffer.size(); l++) {
                        buttonArray.get(Integer.valueOf(gamedata.buttonBuffer.get(l).getId())).setStyle("-fx-text-fill: green; -fx-background-color: white");
                        correctGuesses.add(gamedata.stringBuffer);
                        if (!gamedata.stringBuffer.equals("")) {
                            scoreBox.getChildren().add(new Label(gamedata.stringBuffer + " | " + gamedata.stringBuffer.length() * 5));
                            scoreBox.getChildren().get(scoreBox.getChildren().size()-1).getStyleClass().addAll("dark", "medium");
                            scoreBox.getChildren().get(scoreBox.getChildren().size()-1).setStyle("-fx-text-fill: #ADFF2F");
                        }
                        gamedata.clearStringBuffer();
                    }
                    if (gamedata.getCurrentScore() == gamedata.getTargetScore()) {
                        gamedata.isWon = true;
                        clearBuffers();
                    }
                }
                clearBuffers();
            });

            //When the mouse drag enters a button
            buttonArray.get(i).setOnDragDetected(e -> {
                if (!tempButton.getStyle().equals("-fx-text-fill: green; -fx-background-color: white")) {
                    //If the button is surrounding the current one, then add it to the buffer, else don't do anything
                    currentSurrounding = findSurrounding(tempButton);

                    boolean isEqual = false;
                    for (int j = 0; j < currentSurrounding.size(); j++) {
                        String tempButtonText = tempButton.getText();
                        String otherButtonText = buttonArray.get(currentSurrounding.get(j)).getText();
                        if (tempButtonText.equals(otherButtonText) && !buttonArray.get(currentSurrounding.get(j)).getStyle().equals("-fx-text-fill: red; -fx-background-color: white")) {
                            isEqual = true;
                            break;
                        }
                    }
                    if (isEqual) {
                        tempButton.setStyle("-fx-text-fill: red; -fx-background-color: white");
                        gamedata.addToButtonBuffer(tempButton);
                        System.out.println(gamedata.stringBuffer);
                    }
                    tempButton.startFullDrag();
                }
            });

            buttonArray.get(i).setOnMouseDragEntered(e -> {
                if (!tempButton.getStyle().equals("-fx-text-fill: green; -fx-background-color: white")) {
                    boolean isEqual = false;
                    for (int j = 0; j < currentSurrounding.size(); j++) {
                        String tempButtonText = tempButton.getId();
                        String otherButtonText = buttonArray.get(currentSurrounding.get(j)).getId();
                        if (tempButtonText.equals(otherButtonText) && !buttonArray.get(currentSurrounding.get(j)).getStyle().equals("-fx-text-fill: red; -fx-background-color: white")) {
                            isEqual = true;
                            break;
                        }
                    }
                    if (isEqual) {
                        currentSurrounding = findSurrounding(tempButton);
                        tempButton.setStyle("-fx-text-fill: red; -fx-background-color: white");
                        gamedata.addToStringBuffer(tempButton.getText());
                        gamedata.addToButtonBuffer(tempButton);
                        System.out.println(gamedata.stringBuffer);
                    }
                }
            });

            //When the mouse drag is released
            buttonArray.get(i).setOnMouseDragReleased(e -> {
                if (!tempButton.getStyle().equals("-fx-text-fill: green; -fx-background-color: white")) {
                    for (int j = 0; j < buttonArray.size(); j++) {
                        if (!buttonArray.get(j).getStyle().equals("-fx-text-fill: green; -fx-background-color: white")) {
                            buttonArray.get(j).setStyle("-fx-text-fill: black; -fx-background-color: #9EA5BC");
                        }
                    }
                    tempButton.setStyle("-fx-text-fill: black");

                    //This is the target word
                    if (gamedata.checkStringBuffer(tempList)) {
                        for (int l = 0; l < gamedata.buttonBuffer.size(); l++) {
                            buttonArray.get(Integer.valueOf(gamedata.buttonBuffer.get(l).getId())).setStyle("-fx-text-fill: green; -fx-background-color: white");
                            correctGuesses.add(gamedata.stringBuffer);
                            if (!gamedata.stringBuffer.equals("")) {
                                scoreBox.getChildren().add(new Label(gamedata.stringBuffer + " | " + gamedata.stringBuffer.length() * 5));
                                scoreBox.getChildren().get(scoreBox.getChildren().size()-1).getStyleClass().addAll("dark", "medium");
                                scoreBox.getChildren().get(scoreBox.getChildren().size()-1).setStyle("-fx-text-fill: #ADFF2F");
                            }
                            gamedata.clearStringBuffer();
                        }
                        if (gamedata.getCurrentScore() == gamedata.getTargetScore()) {
                            gamedata.isWon = true;
                            clearBuffers();
                        }
                    }
                    gamedata.clearButtonBuffer();
                }
                clearBuffers();
            });
//=============================== KEYBOARD PRESS HANDLERS BELOW ===============================
            gamedata.appTemplate.getGUI().getWindow().getScene().setOnKeyPressed(e -> {
                if (gamedata.getCurrentWorkspace() == 3) {
                    numOfKeys++;
                    if (e.getCode().toString().equals("ENTER")){
                        System.out.println("Enter pressed");
                        String stringBuffer = new String();
                        for (int z = 0; z < keyboardBuffers.size(); z++){
                            stringBuffer += "\n";
                            if (!keyboardBuffers.get(z).isEmpty()){
                                for (int j = 0; j < keyboardBuffers.get(z).size(); j++){
                                    stringBuffer += keyboardBuffers.get(z).get(j).getText();
                                }
                            }
                        }
                        //CHECKS FOR CORRECT GUESSES
                        for (int f = 0; f < gamedata.levelTargets.size(); f++) {
                            if (stringBuffer.contains(gamedata.levelTargets.get(f))) {  //NEED TO FIX THIS
                                System.out.println(correctGuesses);
                                correctGuesses.add(stringBuffer);
                                scoreBox.getChildren().add(new Label(stringBuffer + " | " + stringBuffer.length()*5));
                                gamedata.addToCurrentScore(stringBuffer.length()*5);
                            }
                        }

                        //IF SCORE == TARGET, END LEVEL
                        if (gamedata.getCurrentScore() == gamedata.getTargetScore()) {
                            gamedata.isWon = true;
                        }

                    } else {
                        for (int j = 0; j < buttonArray.size(); j++) {
                            if (buttonArray.get(j).getText().equals(e.getCode().toString().toLowerCase())) {
                                if (numOfKeys == 1) { //FIRST ONE
                                    buttonArray.get(j).setStyle("-fx-text-fill: red; -fx-background-color: white");
                                    gamedata.addToButtonBuffer(buttonArray.get(j));

                                    //Adds an arraylist of buttons to the keyboardBuffers arraylist
                                    ArrayList<Button> tempArrayList = new ArrayList<>();
                                    tempArrayList.add(buttonArray.get(j));
                                    keyboardBuffers.add(tempArrayList);

                                } else { //ALREADY HAVE ONE LETTER IN THE BUFFER
                                    for (int l = 0; l < keyboardBuffers.size(); l++) {
                                        String tempId = keyboardBuffers.get(l).get(keyboardBuffers.get(l).size() - 1).getId();    //Get most recently added node to check surrounding for
                                        ArrayList<Button> listOfSurroundingButtons = findSurroundingKey(buttonArray, buttonArray.get(Integer.parseInt(tempId)));

                                        int tempLength = listOfSurroundingButtons.size();
                                        if (listOfSurroundingButtons.contains(buttonArray.get(j))) {
                                            for (int m = 0; m < tempLength; m++) {
                                                if (listOfSurroundingButtons.get(m).equals(buttonArray.get(j)) && !keyboardBuffers.get(l).contains(listOfSurroundingButtons.get(m))) {
                                                    //listOfSurroundingButtons.get(m).setStyle("-fx-text-fill: red; -fx-background-color: white");
                                                    //keyboardBuffers.get(l).add(listOfSurroundingButtons.get(m));
                                                    buttonArray.get(Integer.parseInt(listOfSurroundingButtons.get(m).getId())).setStyle("-fx-text-fill: red; -fx-background-color: white");
                                                    keyboardBuffers.get(l).add(buttonArray.get(Integer.parseInt(listOfSurroundingButtons.get(m).getId())));
                                                    System.out.println("UPDATED: " + keyboardBuffers.toString());
                                                }
                                            }

                                        //HOW TO FIND OUT WHEN TO DELETE?!!?!?!?
                                        } else {
                                            ArrayList<String> surroundingLabels = new ArrayList<>();
                                            for (int b = 0; b < listOfSurroundingButtons.size(); b++){
                                                surroundingLabels.add(listOfSurroundingButtons.get(b).getText());
                                            }

                                            if (surroundingLabels.contains(buttonArray.get(j).getText())){
                                                System.out.println("Just incorrect position.");

                                            } else {
                                                for (int z = 0; z < keyboardBuffers.size(); z++){
                                                    for (int k = 0; k < keyboardBuffers.get(z).size(); k++){
                                                        buttonArray.get(Integer.parseInt((keyboardBuffers.get(z)).get(k).getId())).setStyle("-fx-text-fill: black; -fx-background-color: #9EA5BC");

                                                        keyboardBuffers.get(z).clear();
                                                        keyboardBuffers.remove(z);
                                                        System.out.println("REMOVED: " + keyboardBuffers.toString());
                                                        if (keyboardBuffers.isEmpty()) {
                                                            numOfKeys = 0;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            });
        }

        int position = 1;
        for (int i = 0; i < GRID_WIDTH; i++) {        //row (y)
            for (int j = 0; j < GRID_WIDTH; j++) {    //column (x)
                StackPane tempStack = new StackPane(buttonArray.get(position - 1));
                gameplayPane.add(tempStack, j, i);
                position++;
            }
        }
    }

    public void updateScoreBox(VBox otherBox){
        otherBox.getChildren().clear();
        for (int i = 0; i <= this.scoreBox.getChildren().size(); i++){
            if (!((Label)this.scoreBox.getChildren().get(i)).getText().equals(" | 0")) {
                otherBox.getChildren().add(this.scoreBox.getChildren().get(i));
            }
        }
    }

    public VBox getScoreBox(){
        return scoreBox;
    }


    public void clearBuffers(){
        gamedata.clearStringBuffer();
        gamedata.clearButtonBuffer();
        this.currentSurrounding.clear();
    }

    public void resetGameplayGrid(AppDataComponent data) {
        GameData gamedata = (GameData) data;
        gamedata.levelTargets.clear();
        targetScore  = 0;
        currentScore = 0;
    }

    //Fills empty cells with random letters after target words are placed
    public void fillEmptyCells(){
        int randomLetter;
        for (int i = 0; i < buttonArray.size(); i++){
            solvedArray.add(buttonArray.get(i).getText());
            if (buttonArray.get(i).getText().equals("-1")){
                randomLetter = ((int)(Math.random()*25)) + 65;
                buttonArray.get(i).setText((String.valueOf((char)randomLetter).toLowerCase()));
            }
        }
    }

    //Finds the starting position
    public int findOpenPos() {
        int randomCell = (int)(Math.random()*((GRID_WIDTH*GRID_WIDTH) - 1));

        if (buttonArray.get(randomCell).getText().equals("-1")){
            return randomCell;
        } else {
            return findOpenPos();
        }
    }

    //Overloaded the other one (USE THIS FOR THE START POSITION)
    public boolean buildWord(String word, int index){
        return buildWord(word, 0, index);
    }

    //Recursively builds the target word into the grid
    public boolean buildWord(String word, int character, int index){
        char[] wordArray = word.toCharArray();
        buttonArray.get(index).setText(Character.toString(wordArray[character]));
        if (word.length()-1 == character){ return true; }
        ArrayList <Integer> indicesList = findSurrounding(index);
        while (indicesList.size() != 0){
            int randomIndex = (int)(Math.random()*(indicesList.size()-1));
            if (buildWord(word, character + 1, indicesList.get(randomIndex))){
                return true;
            } else {
                buttonArray.get(index).setText("-1");
                buildWord(word, character, findOpenPos());
            }
        }
        return false;
    }


    //Finds the surrounding cells and adds them to the array list
    public ArrayList findSurrounding(int currentPos){
        ArrayList newList = new ArrayList();
        int row = currentPos/GRID_WIDTH;
        int col = currentPos%GRID_WIDTH;

        for (int i = -1; i <= 1; i++){
            if (row + i < 0 || row + i >= GRID_WIDTH)    { continue; }
            for (int j = -1; j <= 1; j++){
                if (col + j < 0 || col + j >=GRID_WIDTH) { continue; }
                if (i == 0 && j == 0)                    { continue; }
                int newIndex = (GRID_WIDTH * (row + i)) + (col + j);
                if (buttonArray.get(newIndex).getText().equals("-1")){
                    newList.add(newIndex);
                }
            }
        }
        return newList;
    }

    public ArrayList findSurrounding(Button button){
        ArrayList newList = new ArrayList();
        int currentPos = Integer.parseInt(button.getId());
        int row = currentPos/GRID_WIDTH;
        int col = currentPos%GRID_WIDTH;

        for (int i = -1; i <= 1; i++){
            if (row + i < 0 || row + i >= GRID_WIDTH)    { continue; }
            for (int j = -1; j <= 1; j++){
                if (col + j < 0 || col + j >=GRID_WIDTH) { continue; }
                if (i == 0 && j == 0)                    { continue; }
                int newIndex = (GRID_WIDTH * (row + i)) + (col + j);
                newList.add(newIndex);
            }
        }
        newList.add(currentPos);
        return newList;
    }

    public ArrayList<Button> findSurroundingKey(ArrayList<Button> buttonArray, Button button){
        ArrayList<Button> newList = new ArrayList();
        int currentPos = Integer.parseInt(button.getId());
        int row = currentPos/GRID_WIDTH;
        int col = currentPos%GRID_WIDTH;

        for (int i = -1; i <= 1; i++){
            if (row + i < 0 || row + i >= GRID_WIDTH)    { continue; }
            for (int j = -1; j <= 1; j++){
                if (col + j < 0 || col + j >=GRID_WIDTH) { continue; }
                if (i == 0 && j == 0)                    { continue; }
                int newIndex = (GRID_WIDTH * (row + i)) + (col + j);
                newList.add(buttonArray.get(newIndex));
            }
        }
        newList.add(buttonArray.get(currentPos));
        return newList;
    }


    public GridPane getGameplayPane() {
        return this.gameplayPane;
    }

    public int getTargetScore(){
        return targetScore;
    }

}
