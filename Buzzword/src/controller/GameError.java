package controller;

/**
 * @author Andrew Raymond
 */
public class GameError extends Error {

    public GameError(String message) {
        super(message);
    }
}
