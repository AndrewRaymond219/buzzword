/*
Categories range from 0-3:
     0 = Places
     1 = English Dictionary
     2 = Science
     3 = Famous People
*/
package gui;

/**
 * Created by Andrew on 11/9/2016.
 */

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.BuzzwordController;

import data.GameData;
import data.GameDataFile;
import data.GameplayGrid;
import data.UserProfile;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import propertymanager.PropertyManager;
import ui.AppGUI;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static buzzword.BuzzwordProperties.*;
import static settings.AppPropertyType.APP_LOGO;
import static settings.InitializationParameters.APP_IMAGEDIR_PATH;

/**
 * This class serves as the GUI component for the Buzzword game.
 *
 * @author Andrew Raymond
 */
public class Workspace extends AppWorkspaceComponent {
    public int       CurrentCategory = 0;  //0 = Places, 1 = English Dictionary, 2 = Science, 3 = Famous People
    public int       currentWorkspace = 0;
    public String    currentCategory;
    GameplayGrid     gameplayGrid;
    boolean          gameOver = false;
    boolean          loggedIn = false;

    //UI Overhaul:
    AppTemplate         app;                // the actual application
    AppGUI              gui;                // the GUI inside which the application sits
    BuzzwordController  controller;
    Label               guiHeadingLabel;
    VBox                titleHBox;            //container to display the heading (previously headPane)
    ArrayList<Circle>   logoCircleArray;
    GridPane            logoCirclePane;
    Button              xButton;

    //Menu Stuff
    BorderPane          menuPane;           //Container for the menu
    BorderPane          menuALPane;
    VBox                menuVBox;
    Button              createProfileButton;
    Button              helpButton;
    Button              loginButton;

    VBox                menuAfterLoginVBox; //Has selectMode MenuButton with dropdown options (categories)
    Button              startPlaying;
    MenuItem            categoryEnglish;
    MenuItem            categoryPlaces;
    MenuItem            categoryScience;
    MenuItem            categoryFamous;
    MenuButton          categoryDropdown;

    //Level Select Stuff
    BorderPane          levelSelectPane;    //Container for the login
    Button              homeButton;
    Button              Level1;
    Button              Level2;
    Button              Level3;
    Button              Level4;
    Button              profileButton;
    VBox                levelSelectLeft;    //Container for homeButton
    HBox                levelSelectRight;
    Label               categoryTitle;
    VBox                levelSelectRCont;

    //Gameplay Stuff
    BorderPane          gameplayPane;       //Container for gameplay

    VBox                gameplayCenter;     //Center Stuff
    GridPane            gameplayCirclePane;
    Label               catTitle;
    Label               levelNum;
    Button              playButton;
    //Button            restartButton;
    Boolean             gamePaused = false;
    ArrayList<Circle>   circleData;
    int                 diff = 0;

    VBox                gameplayRight;      //Right Stuff
    Label               timeRem;
    VBox                scoreBox;
    Label               targetScore;
    Label               totalScore;
    Label               targetWord;
    Timeline            timeline;
    Integer             timeSeconds;
    Integer             STARTTIME;
    Timer               timer;
    TimerTask           newTask;

    //Userscreen stuff
    BorderPane          userdataPane;
    Button              changeProfileName;
    Label               unLabel;
    VBox                userdataCenter;
    VBox                userdataLeft;
    Label               userdataName;

    //HelpScreen stuff
    BorderPane          helpPane;


    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (BuzzwordController) gui.getFileController();

        gui.getWindow().setOnCloseRequest(e->{
            e.consume();

            if (currentWorkspace == 3) { setPause(true); }
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            ImageView imageView = new ImageView();
            imageView.setFitHeight(70);
            imageView.setFitWidth(70);
            imageView.setPreserveRatio(true);

            PropertyManager propertyManager = PropertyManager.getManager();
            URL imgDirURL = AppTemplate.class.getClassLoader().getResource(APP_IMAGEDIR_PATH.getParameter());
            try (InputStream appLogoStream = Files.newInputStream(Paths.get(imgDirURL.toURI()).resolve(propertyManager.getPropertyValue(APP_LOGO)))) {
                imageView.setImage(new Image(appLogoStream));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            alert.setGraphic(imageView);

            alert.setTitle("Quit Buzzword");
            alert.setHeaderText("Are you sure you want to quit Buzzword?");
            alert.initOwner(gui.getWindow());

            Optional<ButtonType> res = alert.showAndWait();
            if (res.get() == ButtonType.OK){
                Platform.exit();
            }
        });

        init();
        setupHandlers();                                             // ... and set up event handling
        switchWorkspace(0);
    }

    //Initializes all of the 3 borderpanes with their GUI's ( maybe can replace layoutGUI()? )
    public void init(){
        //Menu:
        guiHeadingLabel             = new Label("Buzzword");
        xButton                     = new Button("Quit to Desktop");
        titleHBox                   = new VBox(guiHeadingLabel); //Got rid of xButton
        guiHeadingLabel.getStyleClass().add("heading-label");
        titleHBox.getStyleClass().add("outline-border");

        loggedIn                    = false;
        logoCirclePane              = new GridPane();
        logoCircleArray             = new ArrayList(16);

        for (int i = 0; i < 16; i++){    //Fills the circleData ArrayList with the temp circles with id of "H" and radius 5
            Circle circle = new Circle();
            circle.setRadius(25);
            circle.setFill(Color.web("#716F83"));
            logoCircleArray.add(circle);
        }
        logoCircleArray.get(0).setId("B");
        logoCircleArray.get(1).setId("U");
        logoCircleArray.get(4).setId("Z");
        logoCircleArray.get(5).setId("Z");
        logoCircleArray.get(10).setId("W");
        logoCircleArray.get(11).setId("O");
        logoCircleArray.get(14).setId("R");
        logoCircleArray.get(15).setId("D");

        int logoPosition = 1;
        for (int i = 0; i < 4; i++){        //row (y)
            for (int j = 0; j < 4; j++){    //column (x)
                Label tempLabel = new Label(logoCircleArray.get(logoPosition-1).getId());
                tempLabel.getStyleClass().add("white-text");
                StackPane tempStack = new StackPane(logoCircleArray.get(logoPosition-1), tempLabel);
                logoCirclePane.add(tempStack, j, i);
                logoPosition++;
            }
        }
        logoCirclePane.setAlignment(Pos.CENTER);
        logoCirclePane.setVgap(25);
        logoCirclePane.setHgap(25);

        titleHBox.setAlignment(Pos.TOP_CENTER);

        //Menu Inits:
        menuPane                    = new BorderPane();
        homeButton                  = new Button("Home");
        createProfileButton         = new Button("Create Profile");
        loginButton                 = new Button("Log In");
        menuVBox                    = new VBox(createProfileButton, loginButton);

        //Menu After Logged In Inits:
        startPlaying        = new Button("Start Playing");
        categoryEnglish     = new MenuItem("English Dictionary");
        categoryPlaces      = new MenuItem("Places");
        categoryScience     = new MenuItem("Science");
        categoryFamous      = new MenuItem("Famous People");
        categoryDropdown    = new MenuButton();
        menuALPane          = new BorderPane();
        menuAfterLoginVBox  = new VBox(startPlaying, categoryDropdown);

        //Level Select Inits:
        profileButton       = new Button("Filler");
        helpButton          = new Button("Help");
        categoryTitle       = new Label("English Dictionary");
        Level1              = new Button("1");
        Level2              = new Button("2");
        Level3              = new Button("3");
        Level4              = new Button("4");

        levelSelectPane     = new BorderPane();
        levelSelectLeft     = new VBox(profileButton, helpButton);
        levelSelectRight    = new HBox(Level1, Level2, Level3, Level4);
        levelSelectRCont    = new VBox(categoryTitle, levelSelectRight);

        //Gameplay Inits: (use a TableView) for scroll
        playButton          = new Button("Pause");
        //restartButton       = new Button("Restart");
        //buttonHBox          = new HBox(playButton, restartButton);
        catTitle            = new Label("English Dictionary");
        levelNum            = new Label("Level 1");
        gameplayCirclePane  = new GridPane();
        circleData          = new ArrayList(16);

        for (int i = 0; i < 16; i++){    //Fills the circleData ArrayList with the temp circles with id of "H" and radius 5
            Circle circle = new Circle();
            circle.setRadius(25);
            circle.setFill(Color.web("#716F83"));
            circleData.add(circle);
        }

        //TESTING LABELS ON CIRCLES
        circleData.get(0).setId("B");
        circleData.get(0).setStroke(Color.WHITE);
        circleData.get(0).setStrokeWidth(3);

        circleData.get(1).setId("U");
        circleData.get(1).setStroke(Color.WHITE);
        circleData.get(1).setStrokeWidth(3);

        circleData.get(2).setId("B");
        circleData.get(2).setStroke(Color.WHITE);
        circleData.get(2).setStrokeWidth(3);

        circleData.get(4).setId("Z");
        circleData.get(5).setId("Z");
        circleData.get(10).setId("W");
        circleData.get(11).setId("O");
        circleData.get(13).setId("A");
        circleData.get(14).setId("R");
        circleData.get(15).setId("D");
        //TESTING DONE

        //Populates the 4x4 GridPane with the circles from the circleData ArrayList[0-15]
        int position = 1;
        for (int i = 0; i < 4; i++){        //row (y)
            for (int j = 0; j < 4; j++){    //column (x)
                Label tempLabel = new Label(circleData.get(position-1).getId());
                tempLabel.getStyleClass().add("white-text");
                StackPane tempStack = new StackPane(circleData.get(position-1), tempLabel);
                gameplayCirclePane.add(tempStack, j, i);
                position++;
            }
        }

        timeRem             = new Label("TIME REMAINING:  X seconds");
        scoreBox            = new VBox(new Label("GUESSED WORDS | SCORE"));
        scoreBox.getChildren().get(0).getStyleClass().addAll("dark", "white-text");
        targetScore         = new Label("TARGET SCORE:  ");
        totalScore          = new Label("TOTAL SCORE:  ");
        targetWord          = new Label("");

        gameplayRight       = new VBox(timeRem, targetWord, scoreBox, totalScore, targetScore);
        gameplayRight.getStyleClass().add("control-border");
        gameplayRight.setStyle("-fx-background-color: #9EA5BC;");
        gameplayCenter      = new VBox(catTitle, gameplayCirclePane, levelNum, playButton);
        gameplayPane        = new BorderPane();

        totalScore.getStyleClass().addAll("white-text", "dark");

        STARTTIME   = new Integer(60); //TIME
        timeSeconds = STARTTIME;

        //Userdata Screen:
        unLabel             = new Label("PLACEHOLDER");
        userdataCenter      = new VBox(unLabel);
        userdataPane        = new BorderPane();
        userdataLeft        = new VBox();
        changeProfileName   = new Button("Change profile name");
    }

    /* Where scene number is:
           0 = menu scene
           1 = menu scene (after logged in)
           2 = level select scene
           3 = gameplay scene
    */
    public void switchWorkspace(int sceneNumber){
        GameData gamedata = (GameData) app.getDataComponent();
        currentWorkspace = sceneNumber;
        gamedata.setCurrentWorkspace(sceneNumber);
        if (sceneNumber == 0) { //Menu
            if (!menuVBox.getChildren().contains(loginButton)){
                menuVBox.getChildren().add(loginButton);
            }

            menuPane.getStyleClass().add("dark");
            menuPane.setLeft(menuVBox);
            menuPane.setCenter(logoCirclePane);
            menuPane.setPrefWidth(1920);
            menuPane.setPrefHeight(1080);
            menuVBox.setAlignment(Pos.CENTER);
            menuPane.getLeft().getStyleClass().addAll("light", "control-border");
            menuPane.setTop(titleHBox);

            gui.getPrimaryScene().setRoot(menuPane);

        } else if (sceneNumber == 1){   //Menu After Login
            if (!menuAfterLoginVBox.getChildren().contains(loginButton)){
                menuAfterLoginVBox.getChildren().add(loginButton);
            }
            menuALPane.getStyleClass().add("dark");
            categoryDropdown.setText("Categories");
            categoryDropdown.getItems().setAll(categoryPlaces, categoryEnglish, categoryScience, categoryFamous);
            menuAfterLoginVBox.setAlignment(Pos.CENTER);
            menuALPane.setLeft(menuAfterLoginVBox);
            menuALPane.setPrefWidth(1920);
            menuALPane.setPrefHeight(1080);
            menuALPane.setCenter(logoCirclePane);
            menuALPane.getLeft().getStyleClass().addAll("light", "control-border");
            menuALPane.setTop(titleHBox);

            gui.getPrimaryScene().setRoot(menuALPane);

        } else if (sceneNumber == 2){   //Level Select
            if (!levelSelectLeft.getChildren().contains(homeButton)) {
                levelSelectLeft.getChildren().add(homeButton);
            }
            levelSelectPane.getStyleClass().add("dark");
            levelSelectLeft.setAlignment(Pos.CENTER);
            levelSelectLeft.setMaxWidth(175);
            loginButton.setText("Log out of: " + gamedata.getWorkingProfile().getUsername());


            levelSelectRight.setSpacing(50);
            levelSelectRight.setAlignment(Pos.CENTER);
            categoryTitle.getStyleClass().add("title");
            categoryTitle.setAlignment(Pos.CENTER);

            levelSelectRCont.setMinWidth(500);
            levelSelectRCont.setSpacing(250);
            levelSelectRCont.setAlignment(Pos.CENTER);

            levelSelectPane.setLeft(levelSelectLeft);
            levelSelectPane.setCenter(levelSelectRCont);

            levelSelectPane.setPrefWidth(1920);
            levelSelectPane.setPrefHeight(1080);
            levelSelectPane.getLeft().getStyleClass().addAll("light", "control-border");
            levelSelectPane.getCenter().getStyleClass().add("dark");

            gui.getPrimaryScene().setRoot(levelSelectPane);
            levelSelectPane.setTop(titleHBox);

        } else if (sceneNumber == 3) {   //Gameplay
            gamedata.setCurrentScore(0);
            gameplayPane.getStyleClass().add("dark");
            gameplayPane.setLeft(levelSelectLeft);
            catTitle.setAlignment(Pos.TOP_CENTER);
            levelNum.setAlignment(Pos.BOTTOM_CENTER);
            catTitle.getStyleClass().add("title");
            levelNum.getStyleClass().add("title");
            gameplayCenter.setAlignment(Pos.CENTER);
            gameplayPane.setTop(titleHBox);


            gameplayCirclePane.setAlignment(Pos.CENTER);
            gameplayCirclePane.setVgap(25);
            gameplayCirclePane.setHgap(25);
            gameplayCenter.setSpacing(10);
            gameplayPane.setCenter(gameplayCenter);

            timeRem.getStyleClass().add("time-remaining");
            targetScore.getStyleClass().addAll("dark", "white-text");
            scoreBox.getStyleClass().addAll("dark", "white-text", "larger");
            targetScore.setAlignment(Pos.TOP_RIGHT);
            targetWord.getStyleClass().addAll("dark", "white-text");
            targetWord.setAlignment(Pos.BOTTOM_RIGHT);

            gameplayRight.setSpacing(15);
            gameplayPane.setRight(gameplayRight);
            ((Label)(gameplayRight.getChildren().get(4))).setText("Target Score: " + gamedata.getTargetScore());
            (gameplayRight.getChildren().get(4)).getStyleClass().addAll("dark", "white-text");
            gui.getPrimaryScene().setRoot(gameplayPane);
            startGame();

        } else if (sceneNumber == 4){
            //Player Account Screen
            //ADD STUFF ABOUT LEVEL UNLOCKS/HIGH SCORE?...ALLOW TO CHANGE UN OR PASSWORD?
            unLabel.setText("Max level unlocked for " + currentCategory + ": " + gamedata.getMaxLevel(CurrentCategory));
            unLabel.getStyleClass().setAll("larger", "white-text");
            userdataLeft.setAlignment(Pos.CENTER);
            userdataLeft.setMaxWidth(175);
            userdataCenter.setPadding(new Insets(10, 10, 10, 10));
            if (!userdataCenter.getChildren().contains(changeProfileName)) {
                userdataCenter.getChildren().add(changeProfileName);
            }
            userdataPane.setCenter(userdataCenter);
            userdataCenter.setSpacing(10);
            userdataPane.getStyleClass().add("dark");
            userdataLeft.getChildren().add(homeButton);
            userdataName = new Label("User: " + gamedata.getWorkingProfile().getUsername());
            userdataName.getStyleClass().addAll("heading-label");
            HBox userHBox = new HBox(userdataName);
            userHBox.setAlignment(Pos.CENTER);
            userHBox.getStyleClass().add("outline-border");
            userdataPane.setTop(userHBox);
            userdataPane.setLeft(userdataLeft);
            userdataPane.getLeft().getStyleClass().addAll("light", "control-border");

            gui.getPrimaryScene().setRoot(userdataPane);

        } else {

        }
    }

    public void startGame() {
        gameOver = false;
        disableGameplay(false);
        GameData gamedata = (GameData) app.getDataComponent();
        gamedata.isWon = false;
        IntegerProperty timeSeconds = new SimpleIntegerProperty(STARTTIME);
        timeRem.textProperty().bind(timeSeconds.asString());
        if (timeline != null) {
            timeline.stop();
        }
        timeSeconds.set(STARTTIME);
        timeline = new Timeline();
        timeline.getKeyFrames().add(new KeyFrame(Duration.seconds(STARTTIME + 1), new KeyValue(timeSeconds, 0)));
        timeline.playFromStart();

        timer = new Timer();
        newTask = new TimerTask() {
            @Override
            public void run() {
                while (Integer.parseInt(timeRem.getText()) > 0) {
                    if (gamedata.isWon) {
                        gameOver = true;
                        timer.cancel();
                        timeline.stop();
                        gamedata.gameWon();
                        //GAME HAS BEEN WON
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                gameOverPopup(true);
                            }
                        });
                        break;
                    }
                    if (!(("Total Score: " + gamedata.getCurrentScore()).equals(totalScore.getText()))){
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                updateLabelScore();
                                gameplayRight.getChildren().set(2, gameplayGrid.getScoreBox());
                                scoreBox.getStyleClass().addAll("dark");
                                gameplayGrid.getScoreBox().getStyleClass().addAll("dark", "medium");
                                scoreBox.setStyle("-fx-text-fill: #ADFF2F");
                                gameplayGrid.getScoreBox().setStyle("-fx-text-fill: #ADFF2F");
                            }
                        });
                    }
                }
                if (!gamedata.isWon) {
                    gameOver = true;
                    timeline.stop();
                    timer.cancel();
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            gameplayGrid.displaySolvedArray();
                            gameOverPopup(false);
                        }
                    });
                    gamedata.gameLost();
                }
                timeline.stop();
                timer.cancel();
            }
        };
        if (!gameOver) {
            timer.schedule(newTask, 1000);  //Check every second if the user has won...
        }
    }

    public void updateLabelScore(){
        GameData gamedata = (GameData) app.getDataComponent();
        ((Label)gameplayRight.getChildren().get(3)).setText("Total Score: " + gamedata.getCurrentScore());
    }

    //Either resumes or pauses the timer
    private void resumeTimer(boolean startOrPause){
        if (startOrPause){
            timeline.playFrom(timeline.getCurrentTime());
        } else {
            timeline.pause();
        }
    }

    private void setupHandlers() {
        loginButton.setOnMouseClicked           (e -> {
            if (!loggedIn) {
                loginPopup();
            } else {
                switchWorkspace(0);
                loginButton.setText("Log In");
                loggedIn = false;
            }
        });
        createProfileButton.setOnMouseClicked   (e -> createAccPopup());
        startPlaying.setOnMouseClicked          (e -> switchCategory("Places"));
        helpButton.setOnMouseClicked            (e -> createHelpPopup());
        homeButton.setOnMouseClicked            (e -> {
            if (currentWorkspace != 1) {
                switchWorkspace(1);
            }
        });

        profileButton.setOnMouseClicked         (e -> switchWorkspace(4));

        Level1.setOnMouseClicked                (e -> generateGrid(0));
        Level2.setOnMouseClicked                (e -> generateGrid(1));
        Level3.setOnMouseClicked                (e -> generateGrid(2));
        Level4.setOnMouseClicked                (e -> generateGrid(3));

        playButton.setOnMouseClicked            (e -> setPause(!gamePaused));
        xButton.setOnMouseClicked               (e -> confirmationPopup());
        changeProfileName.setOnMouseClicked     (e -> changeProfilePopup());

        categoryPlaces.setOnAction              (e -> switchCategory("Places"));
        categoryEnglish.setOnAction             (e -> switchCategory("English Dictionary"));
        categoryScience.setOnAction             (e -> switchCategory("Science"));
        categoryFamous.setOnAction              (e -> switchCategory("Famous People"));

        //KEYBOARD SHORTCUTS
        final KeyCombination keyComb1 = new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN);
        gui.getPrimaryScene().addEventHandler(KeyEvent.KEY_RELEASED, e -> {
            if (keyComb1.match(e) && (currentWorkspace == 0 || currentWorkspace == 1)) {
                if (!loggedIn) {
                    loginPopup();
                } else {
                    switchWorkspace(0);
                    loginButton.setText("Log In");
                    loggedIn = false;
                }
            }
        });

        final KeyCombination keyComb2 = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_DOWN);
        gui.getPrimaryScene().addEventHandler(KeyEvent.KEY_RELEASED, e -> {
            if (keyComb2.match(e) && currentWorkspace == 0){
                createAccPopup();
            }
        });

        final KeyCombination keyComb3 = new KeyCodeCombination(KeyCode.S, KeyCombination.CONTROL_DOWN);
        gui.getPrimaryScene().addEventHandler(KeyEvent.KEY_RELEASED, e -> {
            if (keyComb3.match(e) && currentWorkspace == 1){
                switchCategory("Places");
            }
        });

        final KeyCombination keyComb4 = new KeyCodeCombination(KeyCode.D, KeyCombination.CONTROL_DOWN);
        gui.getPrimaryScene().addEventHandler(KeyEvent.KEY_RELEASED, e -> {
            if (keyComb4.match(e) && (currentWorkspace == 2 || currentWorkspace == 3)){
                switchWorkspace(4);
            }
        });

        final KeyCombination keyComb5 = new KeyCodeCombination(KeyCode.H, KeyCombination.CONTROL_DOWN);
        gui.getPrimaryScene().addEventHandler(KeyEvent.KEY_RELEASED, e -> {
            if (keyComb5.match(e) && (currentWorkspace == 2 || currentWorkspace == 3)){
                switchWorkspace(1);
            }
        });
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();
        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    //REINITIALIZE when just starting the game
    public void reinitialize() {

    }

    public void disableGameplay(boolean disable){
        for (int i = 0; i < gameplayGrid.getGameplayPane().getChildren().size(); i++) {
            if (disable){
                playButton.setDisable(true);
                //restartButton.setDisable(true);
                gameplayGrid.getGameplayPane().getChildren().get(i).setMouseTransparent(true);
            }
            else {
                playButton.setDisable(false);
                //restartButton.setDisable(false);
                gameplayGrid.getGameplayPane().getChildren().get(i).setMouseTransparent(false);
            }
        }
    }

    public void createHelpPopup(){
        BorderPane helpPopupPane   = new BorderPane();
        helpPopupPane.setPadding(new Insets(5, 5, 5, 5));

        VBox thisView = new VBox();
        Text instructions = new Text("Goal: highlight all of the target words in the selected level.\n" +
                                     "\nHow: drag over letters until all of the desired letters are highlighted. If it is added to the right, you will see your total score go up. Attempt to reach the target score");
        instructions.setWrappingWidth(275);

        thisView.getChildren().add(instructions);

        ScrollPane scroll = new ScrollPane();
        scroll.setContent(thisView);
        scroll.setVbarPolicy(ScrollPane.ScrollBarPolicy.ALWAYS);
        scroll.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);

        helpPopupPane.setCenter(scroll);

        Button close      = new Button("Close");
        helpPopupPane.setBottom(close);

        Stage helpStage = new Stage();
        helpStage.setResizable(false);
        helpStage.setScene(new Scene(helpPopupPane, 300, 100));
        helpStage.sizeToScene();

        helpStage.initModality(Modality.APPLICATION_MODAL);
        helpStage.show();

        close.setOnAction(e ->
                helpStage.close());
    }


    public void confirmationPopup(){
        if(currentWorkspace == 3) { setPause(true); }
        GridPane confirmPopupPane   = new GridPane();
        confirmPopupPane.setPadding(new Insets(12, 12, 12, 12));
        confirmPopupPane.setHgap(-15);
        confirmPopupPane.setVgap(6);

        Label message   = new Label("Are you sure you want to exit?");
        GridPane.setConstraints(message, 0,0);
        confirmPopupPane.getChildren().add(message);

        Button yes      = new Button("Yes");
        GridPane.setConstraints(yes, 0, 1);
        confirmPopupPane.getChildren().add(yes);

        Button no       = new Button("No");
        GridPane.setConstraints(no, 1, 1);
        confirmPopupPane.getChildren().add(no);

        Stage confirmPopupStage = new Stage();
        confirmPopupStage.setScene(new Scene(confirmPopupPane));

        confirmPopupStage.initModality(Modality.APPLICATION_MODAL);
        confirmPopupStage.show();

        yes.setOnAction(new EventHandler<ActionEvent>(){
            @Override public void handle(ActionEvent e){
                confirmPopupStage.close();
                gui.getWindow().close();
            }
        });
        no.setOnMouseClicked    (e -> confirmPopupStage.close());
    }

    public void changeProfilePopup(){
        GridPane changeProfPane     = new GridPane();
        changeProfPane.setPadding(new Insets(12, 12, 12, 12));
        changeProfPane.setHgap(6);
        changeProfPane.setVgap(6);

        final TextField username        = new TextField();
        username.setPromptText("Enter your new username");
        username.setPrefColumnCount(12);
        username.getText();
        GridPane.setConstraints(username, 0, 0);
        changeProfPane.getChildren().add(username);

        Button confirm   = new Button("Confirm");
        GridPane.setConstraints(confirm, 1, 0);
        changeProfPane.getChildren().add(confirm);

        Button cancel  = new Button("Cancel");
        GridPane.setConstraints(cancel, 1, 1);
        changeProfPane.getChildren().add(cancel);

        Stage changeProfStage = new Stage();
        changeProfStage.setScene(new Scene(changeProfPane));

        changeProfStage.initModality(Modality.APPLICATION_MODAL);
        changeProfStage.show();

        cancel.setOnAction (e -> changeProfStage.close());
        confirm.setOnAction(e -> {
            GameData gamedata = (GameData) app.getDataComponent();
            GameDataFile gameDataFile = (GameDataFile)app.getFileComponent();
            String pathStr = "out\\savedata\\" + gamedata.getWorkingProfile().getUsername() + ".json";
            Path path = Paths.get(pathStr);
            String temp = gamedata.getWorkingProfile().getPassword();
            System.out.println(temp);
            gameDataFile.changeUsername(username.getText(), path);
            gamedata.getWorkingProfile().setUsername(username.getText());
            gameDataFile.storeAcc(gamedata, path);
            gamedata.getWorkingProfile().setPassword(temp);
            System.out.println(gamedata.getWorkingProfile().getPassword());

            try {
                Files.move(path, path.resolveSibling(gamedata.getWorkingProfile().getUsername() + ".json"));
            } catch (IOException x){
                x.printStackTrace();
            }

            profileButton.setText("Data for " + username.getText());
            loginButton.setText("Log out of: " + username.getText());
            userdataName.setText("User: " + username.getText());
            changeProfStage.close();
        });
    }

    public void loginPopup(){
        GridPane loginPopupPane     = new GridPane();
        loginPopupPane.setPadding(new Insets(12, 12, 12, 12));
        loginPopupPane.setHgap(6);
        loginPopupPane.setVgap(6);

        final TextField username        = new TextField();
        username.setPromptText("Enter your username");
        username.setPrefColumnCount(12);
        username.getText();
        GridPane.setConstraints(username, 0, 0);
        loginPopupPane.getChildren().add(username);

        final PasswordField password    = new PasswordField();
        password.setPromptText("Enter your password");
        password.setPrefColumnCount(12);
        password.getText();
        GridPane.setConstraints(password, 0, 1);
        loginPopupPane.getChildren().add(password);

        Button logIn   = new Button("Log in");
        GridPane.setConstraints(logIn, 1, 0);
        loginPopupPane.getChildren().add(logIn);

        Stage loginPopupStage = new Stage();
        loginPopupStage.setScene(new Scene(loginPopupPane));

        loginPopupStage.initModality(Modality.APPLICATION_MODAL);
        loginPopupStage.show();

        logIn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                if (loggedIn == false) {
                    GameData gamedata = (GameData) app.getDataComponent();
                    GameDataFile gameDataFile = (GameDataFile) app.getFileComponent();

                    String pathStr = "out\\savedata\\" + username.getText() + ".json";
                    Path path = Paths.get(pathStr);
                    if (gameDataFile.checkAcc(path)) {
                        if (gameDataFile.checkPassword(gamedata, path, password.getText(), gameDataFile)) {
                            profileButton.setText("Data for " + gamedata.getWorkingProfile().getUsername());
                            System.out.println("Loaded: " + gamedata.getLevelData().toString());
                            loggedIn = true;
                            loginButton.setText("Log out of: " + gamedata.getWorkingProfile().getUsername());
                            switchWorkspace(1);
                            loginPopupStage.close();
                        } else {
                            Label errorLabel = new Label("Incorrect Password!");
                            errorLabel.setStyle("-fx-text-fill: red");
                            GridPane.setConstraints(errorLabel, 1, 1);
                            loginPopupStage.setMinWidth(350);
                            loginPopupPane.getChildren().add(errorLabel);
                            loginPopupStage.show();
                        }

                    } else {    //The account doesn't exist, so say so
                        Label errorLabel = new Label("Username doesn't exist!");
                        errorLabel.setStyle("-fx-text-fill: red");
                        GridPane.setConstraints(errorLabel, 1, 1);
                        loginPopupStage.setMinWidth(350);
                        loginPopupPane.getChildren().add(errorLabel);
                        loginPopupStage.show();
                    }
                } else {
                    loginButton.setText("Log in");
                    switchWorkspace(0);
                }
            }
        });
    }

    public void gameOverPopup(boolean won){
        if (gameOver) {
            gameOver = false;
            GameData gamedata = (GameData) app.getDataComponent();
            disableGameplay(true);
            GridPane gameOverPane = new GridPane();
            gameOverPane.setPadding(new Insets(5, 5, 5, 5));
            gameOverPane.setHgap(6);
            gameOverPane.setVgap(6);

            Stage gameOverStage = new Stage();
            gameOverStage.setScene(new Scene(gameOverPane));
            gameOverStage.setHeight(100);
            gameOverStage.setWidth(250);

            gameOverStage.initModality(Modality.APPLICATION_MODAL);
            gameOverStage.initStyle(StageStyle.UTILITY);
            gameOverStage.show();

            if (won) {
                Button no = new Button("Return to Level Select");
                GridPane.setConstraints(no, 0, 1);
                gameOverPane.getChildren().add(no);

                if (gamedata.getMaxLevel(CurrentCategory) < 4) {
                    Label message = new Label("You won!");
                    GridPane.setConstraints(message, 0, 0);
                    gameOverPane.getChildren().add(message);
                    if (gamedata.getMaxLevel(CurrentCategory)-1 == diff) {
                        gamedata.unlockLevel(CurrentCategory);
                    }

                    Button yes = new Button("Next Level");
                    GridPane.setConstraints(yes, 1, 1);
                    gameOverPane.getChildren().add(yes);

                    yes.setOnMouseClicked(e -> {
                        setPause(false);
                        resetWorkspace(gamedata);
                        generateGrid(gamedata.getMaxLevel(CurrentCategory) - 1);
                        gameOverStage.close();
                    });
                } else {
                    Label message = new Label("Completed category!");
                    GridPane.setConstraints(message, 0, 0);
                    gameOverPane.getChildren().add(message);
                    if (gamedata.getMaxLevel(CurrentCategory)-1 == diff) {
                        gamedata.unlockLevel(CurrentCategory);
                    }
                }

                gamedata.getWorkingProfile().updateProfile();    //SAVE PROFILE

                no.setOnMouseClicked(e -> {
                    setPause(false);
                    updateButtons(currentCategory);
                    switchWorkspace(2);
                    gameOverStage.close();
                });

            } else {
                Button no = new Button("No");
                GridPane.setConstraints(no, 1, 1);
                gameOverPane.getChildren().add(no);

                Label message = new Label("You lost. Would you like to retry?");
                Button yes = new Button("Yes");
                GridPane.setConstraints(yes, 0, 1);
                gameOverPane.getChildren().add(yes);

                GridPane.setConstraints(message, 0, 0);
                gameOverPane.getChildren().add(message);

                yes.setOnMouseClicked(e -> {
                    timeline.stop();
                    timer.cancel();
                    restartLevel();
                    gameOverStage.close();
                });

                no.setOnMouseClicked(e -> {
                    timeline.stop();
                    timer.cancel();
                    setPause(false);
                    switchCategory(currentCategory);
                    switchWorkspace(2);
                    gameOverStage.close();
                });
            }
        }
    }

    public void updateButtons(String currentCategory){
        GameData gamedata = (GameData) app.getDataComponent();
        int maxLevel = 0;
        Level1.setDisable(false);
        Level2.setDisable(true);
        Level3.setDisable(true);
        Level4.setDisable(true);

        if (currentCategory.equals("Places")) {
            maxLevel = gamedata.getMaxLevel(0);
            CurrentCategory = 0;
        } else if (currentCategory.equals("English Dictionary")) {
            maxLevel = gamedata.getMaxLevel(1);
            CurrentCategory = 1;
        } else if (currentCategory.equals("Science")) {
            maxLevel = gamedata.getMaxLevel(2);
            CurrentCategory = 2;
        } else if (currentCategory.equals("Famous People")) {
            maxLevel = gamedata.getMaxLevel(3);
            CurrentCategory = 3;
        }

        if    (maxLevel == 2){
            Level2.setDisable(false);
        }  if (maxLevel == 3){
            Level2.setDisable(false);
            Level3.setDisable(false);
        }  if (maxLevel == 4){
            Level2.setDisable(false);
            Level3.setDisable(false);
            Level4.setDisable(false);
        }
    }


    public void createAccPopup(){
        GridPane createAccPane     = new GridPane();
        createAccPane.setPadding(new Insets(12, 12, 12, 12));
        createAccPane.setHgap(6);
        createAccPane.setVgap(6);

        final TextField username        = new TextField();
        username.setPromptText("Create desired username");
        username.setPrefColumnCount(12);
        username.getText();
        GridPane.setConstraints(username, 0, 0);
        createAccPane.getChildren().add(username);

        final PasswordField password    = new PasswordField();
        password.setPromptText("Create desired password");
        password.setPrefColumnCount(12);
        password.getText();
        GridPane.setConstraints(password, 0, 1);
        createAccPane.getChildren().add(password);

        Button createAccount   = new Button("Create Account");
        GridPane.setConstraints(createAccount, 1, 0);
        createAccPane.getChildren().add(createAccount);

        Stage createAccStage = new Stage();
        createAccStage.setScene(new Scene(createAccPane));

        createAccStage.initModality(Modality.APPLICATION_MODAL);
        createAccStage.show();

        createAccount.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event){
                //HW5 - Create JSON File with username.getText() as the name of the file, and store the password.getText() in the file
                GameData        gamedata        = (GameData) app.getDataComponent();
                GameDataFile    gameDataFile    = (GameDataFile) app.getFileComponent();
                gamedata.initLevelData();
                UserProfile     profile         =  new UserProfile(username.getText(), password.getText(), gamedata, gameDataFile);  //this will create a new profile and store it
                gamedata.setWorkingProfile(profile);

                //See if the profile has already been stored or not
                if (profile.storeProfile()){
                    switchWorkspace(1);
                    profileButton.setText("Data for " + gamedata.getWorkingProfile().getUsername());
                    loginButton.setText("Logout of: " + gamedata.getWorkingProfile().getUsername());
                    loggedIn = true;
                    createAccStage.close();
                } else {
                    Label errorLabel = new Label("Username already exists!");
                    errorLabel.setStyle("-fx-text-fill: red");
                    GridPane.setConstraints(errorLabel, 1, 1);
                    createAccStage.setMinWidth(350);
                    createAccPane.getChildren().add(errorLabel);
                    createAccStage.show();
                }
            }
        });
    }

    //Switches the category and buttons activated
    public void switchCategory(String categoryName){
        GameData gamedata = (GameData)app.getDataComponent();
        resetWorkspace(gamedata);
        currentCategory = categoryName;
        updateButtons(categoryName);

        //UPDATE THE RIGHT LEVELSELECT PANE HERE with the new buttons.
        levelSelectRight.getChildren().clear();
        levelSelectRCont.getChildren().clear();
        levelSelectRight    = new HBox(Level1, Level2, Level3, Level4);
        levelSelectRCont    = new VBox(categoryTitle, levelSelectRight);

        categoryTitle.setText(categoryName);
        catTitle.setText(categoryName);
        switchWorkspace(2);
    }

    //Categories range from 0-3:    0 = Places, 1 = English Dictionary, 2 = Science, 3 = Famous People
    //Difficulties range from 0-3:  explained in GameplayGrid
    public void generateGrid(int difficulty){
        this.diff = difficulty;
        GameData gamedata = (GameData)app.getDataComponent();
        levelNum.setText("Level " + (difficulty+1));
        System.out.println("Generate a grid for Category #" + CurrentCategory  + " with difficulty of " + (difficulty+1));
        gameplayGrid = new GameplayGrid(gamedata, CurrentCategory, (difficulty+1));    //Creates an instance of the GameplayGrid

        totalScore.setText(totalScore.getText() + gamedata.getCurrentScore());
        targetScore.setText(targetScore.getText() + gameplayGrid.getTargetScore());
        gameplayCenter      = new VBox(catTitle, gameplayGrid.getGameplayPane(), levelNum, playButton);

        switchWorkspace(3);
    }

    public void setPause(boolean pauseGame){
        resumeTimer(!pauseGame);
        if (pauseGame){
            //restartButton.setDisable(true);
            gamePaused = true;
            playButton.setText("Play");
            catTitle.setText("GAME PAUSED");
            catTitle.setStyle("-fx-text-fill: red; -fx-background-color: #9EA5BC");
            gameplayGrid.getGameplayPane().setVisible(false);
        } else {
            //restartButton.setDisable(false);
            gamePaused = false;
            playButton.setText("Pause");
            catTitle.setText(currentCategory);
            catTitle.setStyle("-fx-text-fill: white; -fx-background-color: #868DA1");
            gameplayGrid.getGameplayPane().setVisible(true);
        }
    }

    public void restartLevel(){
        setPause(false);
        generateGrid(Integer.parseInt(levelNum.getText().substring(levelNum.getText().length()-1, levelNum.getText().length())) - 1);
    }

    public void resetWorkspace(GameData gamedata){
        gamedata.levelTargets.clear();
        gamePaused = false;
        playButton.setText("Pause");
        targetScore.setText("Target Score: ");
        totalScore.setText("Total Score: ");
    }
}